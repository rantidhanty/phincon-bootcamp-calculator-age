function onSubmit() {
const dd = document.getElementById("dd").value;
const mm = document.getElementById("mm").value;
const yy = document.getElementById("yy").value;

if (dd && mm && yy) {
    const newDate = new Date(`${yy}-${mm}-${dd}`);
    const today = new Date();

    let ageYears = today.getFullYear() - newDate.getFullYear();
    let ageMonths = today.getMonth() - newDate.getMonth();
    let ageDays = today.getDate() - newDate.getDate();
    // console.log(ageDays);
    // console.log(ageMonths);
    // console.log(ageYears);
    if (ageDays < 0) {
        ageMonths--;
        const lastMonth = new Date(today.getFullYear(), today.getMonth(), 0);
        ageDays += lastMonth.getDate();
    }
    if (ageMonths < 0) {
        ageYears--;
        ageMonths += 12;
        }

        document.getElementById("outputy").textContent = ageYears;
        document.getElementById("outputd").textContent = ageDays;
        document.getElementById("outputm").textContent = ageMonths;
} else {
    alert("please, enter your birthday");
}
}